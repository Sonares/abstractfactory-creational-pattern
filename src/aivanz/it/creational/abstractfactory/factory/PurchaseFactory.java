package aivanz.it.creational.abstractfactory.factory;

import aivanz.it.creational.abstractfactory.product.DeliveryDocument;
import aivanz.it.creational.abstractfactory.product.Packaging;

public abstract class PurchaseFactory {
	public abstract Packaging CreatePackaging();
	public abstract DeliveryDocument CreateDeliveryDocument();
}