package aivanz.it.creational.abstractfactory.factory;

import aivanz.it.creational.abstractfactory.product.DeliveryDocument;
import aivanz.it.creational.abstractfactory.product.Packaging;
import aivanz.it.creational.abstractfactory.product.PostalLabel;
import aivanz.it.creational.abstractfactory.product.StandardPackaging;

public class StandardPurchaseFactory extends PurchaseFactory {
	@Override
	public Packaging CreatePackaging() {
		return new StandardPackaging();
	}

	@Override
	public DeliveryDocument CreateDeliveryDocument() {
		return new PostalLabel();
	}
}