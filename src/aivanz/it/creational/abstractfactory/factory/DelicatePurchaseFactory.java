package aivanz.it.creational.abstractfactory.factory;

import aivanz.it.creational.abstractfactory.product.CourierManifest;
import aivanz.it.creational.abstractfactory.product.DeliveryDocument;
import aivanz.it.creational.abstractfactory.product.Packaging;
import aivanz.it.creational.abstractfactory.product.ShockProofPackaging;

public class DelicatePurchaseFactory extends PurchaseFactory {
	@Override
	public Packaging CreatePackaging() {
		return new ShockProofPackaging();
	}

	@Override
	public DeliveryDocument CreateDeliveryDocument() {
		return new CourierManifest();
	}
}