package aivanz.it.creational.abstractfactory;

import aivanz.it.creational.abstractfactory.factory.PurchaseFactory;
import aivanz.it.creational.abstractfactory.product.DeliveryDocument;
import aivanz.it.creational.abstractfactory.product.Packaging;

public class Client {
	private Packaging _packaging;
	private DeliveryDocument _deliveryDocument;

	public Client(PurchaseFactory factory) {
		_packaging = factory.CreatePackaging();
		_deliveryDocument = factory.CreateDeliveryDocument();
	}

	public Packaging ClientPackaging() {
		return _packaging;
	}

	public DeliveryDocument ClientDocument() {
		return _deliveryDocument;
	}
}