package aivanz.it.creational.abstractfactory.product;

public class StandardPackaging extends Packaging {
	public StandardPackaging() {
		System.out.println("I'm a standard package.");
	}
}
