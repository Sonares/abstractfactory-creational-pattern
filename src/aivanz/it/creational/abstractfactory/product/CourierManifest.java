package aivanz.it.creational.abstractfactory.product;

public class CourierManifest extends DeliveryDocument {
	public CourierManifest() {
		System.out.println("I'm a courier manifest.");
	}
}
