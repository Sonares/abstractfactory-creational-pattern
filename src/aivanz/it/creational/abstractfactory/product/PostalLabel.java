package aivanz.it.creational.abstractfactory.product;

public class PostalLabel extends DeliveryDocument {
	public PostalLabel() {
		System.out.println("I'm a postal label.");
	}
}
