package aivanz.it.creational.abstractfactory.product;

public class ShockProofPackaging extends Packaging {
	public ShockProofPackaging() {
		System.out.println("I'm a shockproof package.");
	}
}
