package aivanz.it.creational.abstractfactory;

import aivanz.it.creational.abstractfactory.factory.DelicatePurchaseFactory;
import aivanz.it.creational.abstractfactory.factory.PurchaseFactory;
import aivanz.it.creational.abstractfactory.factory.StandardPurchaseFactory;

/* Creational pattern, it controls class instantiation. 
 * The abstract factory pattern is used to provide a client with a set of related or dependant objects. 
 * The "family" of objects created by the factory is determined at run-time according to the selection of 
 * concrete factory class.*/

/* Example:
 * Packaging delivery. 
 * The company delivers 2 types of product: 
 * - One simple, standard product, delivered in a standard box and delivered through the post with a simple label.
 * - The second is a delicate item that requires shockproof packaging and is delivered via a courier that requires
 *  a detailed manifest.
 * */

/* 
 * Client:
 * -productA: AbstractProductA
 * -productB: AbstractProductB
 * +Client(factory: AbstractFactory)
 * Main class that instantiates products based on specific factory.
 * 
 * AbstractFactory:
 * +CreateProductA(): AbstractProductA
 * +CreateProductB(): AbstractProductB
 * In this case: PurchaseFactory. An abstract class that specifies the factory used to instantiate the objects. 
 * Pass this in Client construct to instantiates the class that you want. 
 * 
 * ConcreteFactoryA extends AbstractFactory:
 * +CreateProductA(): AbstractProductA
 * +CreateProductB(): AbstractProductB
 * In this case: StandardPurchaseFactory. Concrete class that create an instance of standard package and postal label. 
 * 
 * ConcreteFactoryB extends AbstractFactory:
 * +CreateProductA(): AbstractProductA
 * +CreateProductB(): AbstractProductB
 * In this case: DelicatePurchaseFactory. Concrete class that create an instance of shockproof package and courier manifest.
 * 
 * AbstractProductA:
 * In this case: Packaging. Abstract class that specifies the first "type of product". The client doesn't need to know 
 * the object that it will create, so we make this "middle layer" that simply says "The type is Packaging. What kind of packaging
 * it is, I don't care".
 * 
 * ConcreteProductA1 extends AbstractProductA:
 * In this case: StandardPackaging that extends Packaging. First concrete type of Packaging (AbstractProductA).
 * This is created by the CreatePackaging() method (CreateProductA()) in StandardPurchaseFactory (ConcreteFactoryA). 
 * 
 * ConcreteProductA2 extends AbstractProductA:
 * In this case: ShockProofPackaging that extends Packaging. Second concrete type of Packaging (AbstractProductA).
 * This is created by the CreatePackaging() method (CreateProductA()) in DelicatePurchaseFactory (ConcreteFactoryB). 
 * 
 * AbstractProductB:
 * In this case: DeliveryDocument. Abstract class that specifies the second "type of product". The client doesn't need to know 
 * the object that it will create, so we make this "middle layer" that simply says "The type is DeliveryDocument. What kind of 
 * document it is, I don't care".
 * 
 * ConcreteProductB1 extends AbstractProductB:
 * In this case: PostalLabel that extends DeliveryDocument. First concrete type of DeliveryDocument (AbstractProductB).
 * This is created by the CreateDeliveryDocument() method (CreateProductB()) in StandardPurchaseFactory (ConcreteFactoryA).
 * 
 * ConcreteProductB2 extends AbstractProductB:
 * In this case: CourierManifest that extends DeliveryDocument. Second concrete type of DeliveryDocument (AbstractProductB).
 * This is created by the CreateDeliveryDocument() method (CreateProductB()) in DelicatePurchaseFactory (ConcreteFactoryB).
 * 
 * */
public class MainClass {
	public static void main(String args[]) {

		// This says "I want to create products from StandardPurchaseFactory. 
		// I don't know in particular what objects are, but I want them."
		PurchaseFactory spf = new StandardPurchaseFactory();
		Client standard = new Client(spf);
		standard.ClientPackaging();
		standard.ClientDocument();

		// This says "I want to create products from DelicatePurchaseFactory. 
		// I don't know in particular what objects are, but I want them."
		PurchaseFactory dpf = new DelicatePurchaseFactory();
		Client delicate = new Client(dpf);
		delicate.ClientPackaging();
		delicate.ClientDocument();

	}
}
