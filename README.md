#AbstractFactory
This is a simple example of AbstractFactory, creational design pattern.  

**Description:**  
The abstract factory pattern is used to provide a client with a set of related or dependant objects. The "family" of objects created by the factory are determined at run-time.  

![PlantUML model] (http://www.plantuml.com/plantuml/png/lPInQe0m48RtUugCYaJGiHJH4pht5TAOGi5gIU9GhEzUIoA1N6mpTN7u-Rp_knZiZHNQRkypo5aOmyRvJIsMxS1tyMAi5j8U7tgTDcc71pRuwDad2Ge9XL2vgKM0Ny_7gZy3--I24Ww0Ys9EAIvb-zyGEiXlmtb7rSdOno_EsRWkKYkhhhJ9vDgqKTZi5Ah2_gDM8hnSbFHeRsbCq0W7sJLK0pYZozd7muzIZaXMVTJfJYbkl371VpV1t-u6GXmalpascLmBG8w-hZjwrx8mRCWuJ8vhqnYrwzKodN-0Rm00)  
